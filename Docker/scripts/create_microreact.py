#!/usr/bin/env python3
import requests
import sys
import json

#  read in files from command line arguments
if len(sys.argv) < 4:
    print('Incorrect command format.\nUSAGE: create_microreact.py <MICROREACT ACCESS TOKEN> <MICROREACT CSV> <NEWICK FILE>')
    sys.exit(1)

access_token = sys.argv[1]
microreact_csv = sys.argv[2]
newick_tree = sys.argv[3]

with open(microreact_csv) as file:
    data_string = file.read()

with open(newick_tree) as file:
    tree_string = file.read()

# create new microreact
headers = {
    'Content-type': 'application/json; charset=UTF-8',
    'Access-Token': access_token,
}   

micoreact_json = {
    "name": "SARS-CoV-2",
    "data": data_string,
    "tree": tree_string
}

data = json.dumps(micoreact_json, indent=4)
response = requests.post('https://microreact.org/api/project/', headers=headers, data=data)
json_response = json.loads(response.text)
print(json_response['url'])