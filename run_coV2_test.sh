nx_data=${PWD}

nextflow run main.nf \
	--fasta_file ${nx_data}/test_data/fasta_files/brazil_ncbi.fasta \
	--microreact_metadata ${nx_data}/test_data/metadata.csv \
        --reference ${nx_data}/references/MN908947.3.fasta \
	--config_dir ${nx_data}/type_variants/config.csv \
        --output_dir . \
	--iqtree \
        -resume \
        -profile docker

