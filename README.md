## coV2: SARS-CoV-2 Nextflow Pipeline

This process generate a phylogeny inference, lineages and variants type assignment to sars_cov2 based on mapping to reference Wuhan_Hu_1, quality filters (cov and max-ambig), tree generation (fastree default; iqtree alternative), pangolin and nextclade prediction software and type_variants using nextflow and conda envs to make scalable and easy to use with updated data

### Pre-requisites:

 * conda `pip3 install conda` or
 * Docker


#### Conda

If using conda create an environment named cov2 
```
conda env create -f Docker/conda_environments/environment.yml
```
If conda is already install, please, check conda env path in nextflow.config file `-profile standard`

#### Docker
If using docker, pull sarscov2 container
```
docker pull registry.gitlab.com/johan.bernal.morales/sarscov2:latest
```

#### Software included in Conda environment or Docker container

 * minimap2 2.17
 * datafunk 0.0.8
 * clusterfunk 0.0.3
 * pangolin 2.3.0
 * fasttree 2.1.10
 * iqtree 2.0.3
 * type_variants
 * nextclade

### Running the workflow

```
nextflow run main.nf --fasta_file <PATH TO SARS-CoV-2 CONSENSUS FASTAS> \
                     --config_dir $PWD/type_variants/config.csv \
                     --output_dir <OUTPUT DIRECTORY> \
                     --iqtree \
                     -resume \
                     -profile docker
```
The <PATH TO SARS-CoV-2 CONSENSUS FASTAS> can either be
  * the path to a multifasta file such as `/path/input_dir/multi_consensus.fasta`
  * a glob file pattern match such as `"/path/input_dir/*.fasta"`
  
or edit run_coV2_test.sh


If are running with Docker add `-profile docker` to the command line

#### Making Microreact projects

To create a microreact-compatible csv provide a csv file with the `--microreact_metadata` parameter. This file should have a column `id` where the values correspond to the fasta file heads e.g
 * header is 
`>MT126808.1 Severe acute respiratory syndrome coronavirus 2 isolate SARS-CoV-2/human/BRA/SP02/2020, complete genome` 
 * id is `MT126808.1`

Additional columns can be added such as 
 * `longitude` and `latitute` to add a location
 * `day`, `month`, and `year` to add dates to a timeline for the samples
 * any other data

To create a Microreact project from the compatible csv add the access_token for your account using the `--microreact_access_token` parameter

## Authors:
[Johan Fabian Bernal](https://gitlab.com/johan.bernal.morales) <johan.bernal.morales@gmail.com> <br>
[Varun Shamanna](https://gitlab.com/varunshamanna4) <varunshamanna4@gmail.com> <br>
[Anthony Underwood](https://gitlab.com/antunderwood) <au3@sanger.ac.uk> <br>

Johan F. Bernal is microbiologist with (c)master’s degree in public health from Andes University Bogotá, Colombia. He has been working approximately for 10 years from "one health" scope generating knowledge in antimicrobial resistance and molecular epidemiology of infectious diseases in Colombia. Last 5 years, he has been part of the implementation of genomics to understand antimicrobial resistance and the epidemiology in pathogens of global health concern. 

His working base has been “Corporación Colombiana de Investigación Agropecuaria-AGROSAVIA" involved in the Colombian program for antimicrobial resistance surveillance- COIPARS. He had participated in national and international projects, recently, he is part of NIH Global Health Research Unit, initiative of the Centre for Genomic Pathogen Surveillance (CGPS) at Sanger institute in UK, and for 2 years, he has been bioinformatic area coordinator in AGROSAVIA. He has been involved in different OMS/PAHO pathogens reference networks in latin america (Pulsenet, Relavra). 
